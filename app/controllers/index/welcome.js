import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class IndexWelcomeController extends Controller {
    queryParams = ['step', 'character'];

    @tracked step = 1;
    @tracked character = 'jack';

    get nextStep() {
        return this.step + 1;
    }
}
